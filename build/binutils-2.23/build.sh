#! /bin/sh
## Copyright (c) 2013 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the Binutils package.
##
## Author:
##
##     Evan Green 23-Oct-2013
##
## Environment:
##
##     Build
##

. ../build_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure-tools)
    export AR='ar'
    sh ${SOURCE_DIRECTORY}/configure --target="$TARGET" \
                                     --disable-nls \
                                     --disable-werror \
                                     --prefix="/" \
                                     --with-sysroot="/" \
                                     --disable-shared \
                                     --enable-multilib \
                                     CFLAGS="$CFLAGS"

    ;;

  configure)
    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --disable-nls \
                                     --disable-werror \
                                     --prefix="/usr" \
                                     --with-sysroot="/" \
                                     --disable-shared \
                                     --enable-multilib \
                                     CFLAGS="$CFLAGS"

    ;;

  build-tools)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install-strip prefix="$OUTPUT_DIRECTORY/"
    ;;

  build)
    make $PARALLEL_MAKE tooldir=/usr
    if test "x$BUILD_OS" = "xwin32"; then
        export STRIPPROG="$TARGET-strip"
    fi

    make $PARALLEL_MAKE install prefix="$OUTPUT_DIRECTORY/" \
        tooldir="$OUTPUT_DIRECTORY/usr"

    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

