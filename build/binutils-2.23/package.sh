##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the binutils binaries.
##
## Author:
##
##     Evan Green 12-Feb-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/lib" "$PACKAGE_DIRECTORY/usr"
rm -f "$PACKAGE_DIRECTORY/usr/lib/libiberty.a"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: binutils
Priority: optional
Version: 2.23
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://ftp.gnu.org/gnu/binutils/binutils-2.23.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: A GNU collection of binary utilities
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

