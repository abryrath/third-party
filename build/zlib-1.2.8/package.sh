##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the zlib library.
##
## Author:
##
##     Evan Green 13-Feb-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr/include"
mkdir -p "$PACKAGE_DIRECTORY/usr/lib"
cp -pv "$BUILD_DIRECTORY/libz.a" "$PACKAGE_DIRECTORY/usr/lib"
cp -pv "$BUILD_DIRECTORY/zlib.h" "$PACKAGE_DIRECTORY/usr/include"
cp -pv "$BUILD_DIRECTORY/zconf.h" "$PACKAGE_DIRECTORY/usr/include"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libz
Priority: optional
Version: 1.2.8
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://zlib.net/zlib-1.2.8.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: ZLib compression library.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

