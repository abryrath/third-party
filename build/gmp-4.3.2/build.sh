##
## Copyright (c) 2013 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the GMP package.
##
## Author:
##
##     Evan Green 24-Oct-2013
##
## Environment:
##
##     Build
##

. ../build_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure-tools)
    export AR='ar'

    ##
    ## Determine whether to build a 32 or 64 bit ABI version. Using uname -m
    ## is not good enough, as the machine might be 64 bits while the
    ## compiler is still only 32.
    ##

    ABILINE=
    if test "$BUILD_OS" != "minoca"; then
        if echo | gcc -dM -E - | grep '__SIZEOF_LONG__ 8' ; then
            ABILINE=ABI=64
        else
            ABILINE=ABI=32
        fi
    fi

    ##
    ## Config.guess returns i386 for Mac OS, which is usually wrong.
    ##

    BUILD_LINE=
    if [ "$BUILD_OS" = "macos" ]; then
        BUILD_LINE="--build=x86_64-apple-darwin --host=x86_64-apple-darwin"
    fi

    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --prefix="$OUTPUT_DIRECTORY" \
                                     --enable-static \
                                     --disable-shared \
                                     CFLAGS="$CFLAGS" \
                                     $ABILINE

    ;;

  configure)
    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --enable-static \
                                     --disable-shared \
                                     --host="$TARGET" \
                                     --prefix="/usr" \
                                     CFLAGS="$CFLAGS"

    ;;

  build)
    make $PARALLEL_MAKE

    ##
    ## Create some of the output directories since install-sh seems to die when
    ## run with parallel make.
    ##

    mkdir -p "$OUTPUT_DIRECTORY/usr/include" "$OUTPUT_DIRECTORY/usr/lib" \
        "$OUTPUT_DIRECTORY/usr/share/info"

    make $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY/"
    ;;

  build-tools)
    make $PARALLEL_MAKE

    ##
    ## Create some of the output directories since install-sh seems to die when
    ## run with parallel make.
    ##

    mkdir -p "$OUTPUT_DIRECTORY/include" "$OUTPUT_DIRECTORY/lib" \
        "$OUTPUT_DIRECTORY/share/info"

    make $PARALLEL_MAKE install
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

