##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the automake packages (all versions).
##
## Author:
##
##     Evan Green 19-May-2016
##
## Environment:
##
##     Build with POSIX tools.
##

. ../../build_common.sh

AUTOCONF="autoconf_2.64"

##
## Get the version number (ie 1.8.4).
##

PKG_VERSION=`echo $SOURCE_DIRECTORY | sed -n 's|.*automake-\(.*\)\.src.*|\1|p'`
if [ -z "$PKG_VERSION" ]; then
    echo "Error: PKG_VERSION came up empty."
    exit 1
fi

##
## Automake needs autoconf.
##

if test "x$BUILD_OS" = "xwin32"; then

    ##
    ## Unfortunately Perl backtick commands send each word as a separate
    ## argument, so simply setting it to "sh -c" would get expanded to
    ## "sh -c 'word0' '...'", when it really should be "sh -c 'word0 ...'".
    ## This "script" repackages the command line to be in a single argument.
    ##

    export PERL5SHELL='sh -c "sh -c \\"\$*\\"" dummy'
    export M4="$TOOLBINROOT/bin/m4"
    export TMPDIR="$BUILD_DIRECTORY"
    export ac_cv_path_PERL='perl'
fi

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure-tools)
    export AUTOCONF="$TOOLBINROOT/bin/autoconf-2.64"
    sh ${SOURCE_DIRECTORY}/configure --prefix="$OUTPUT_DIRECTORY" \
                                     --program-suffix=-$PKG_VERSION \

    ;;

  configure)
    extract_dependency "$AUTOCONF"
    export AUTOCONF="$DEPENDROOT/usr/bin/autoconf-2.64"
    export AUTOM4TE="$DEPENDROOT/usr/bin/autom4te-2.64"
    export PERL5LIB="$DEPENDROOT/usr/share/autoconf"
    sed "s|'/usr/share/autoconf'|'$DEPENDROOT/usr/share/autoconf'|" \
      "$DEPENDROOT/usr/share/autoconf/autom4te.cfg" \
      >"$DEPENDROOT/usr/share/autoconf/autom4te.cfg2"

    export AUTOM4TE_CFG="$DEPENDROOT/usr/share/autoconf/autom4te.cfg2"
    sh ${SOURCE_DIRECTORY}/configure --prefix="/usr" \
                                     --program-suffix=-$PKG_VERSION \

    ;;

  build | build-tools)
    make
    make install prefix="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

