##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the autoconf utilities.
##
## Author:
##
##     Evan Green 19-May-2016
##
## Environment:
##
##     Build
##

. ../../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr/share"
cp -Rpv "$BUILD_DIRECTORY/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/share/autoconf" "$PACKAGE_DIRECTORY/usr/share"

##
## Get the version number (ie 2.64).
##

PKG_VERSION=`echo $BUILD_DIRECTORY | sed -n 's|.*autoconf-\(.*\)\.build.*|\1|p'`
if [ -z "$PKG_VERSION" ]; then
    echo "Error: PKG_VERSION came up empty."
    exit 1
fi

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: autoconf
Priority: optional
Version: $PKG_VERSION
Architecture: all
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://ftp.gnu.org/gnu/autoconf/autoconf-$PKG_VERSION.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: GNU auto-configuration scripts.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

