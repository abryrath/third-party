##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the Perl package.
##
## Author:
##
##     Evan Green 8-Jan-2015
##
## Environment:
##
##     Windows Build with POSIX tools.
##

. ../build_common.sh

##
## Presumably because they've reinvented all of autoconf but shakier, Perl
## seems to need to build out of its source directory. The source directory
## will need to be copied wholesale into the build directory.
##

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    if test "x$BUILD_OS" != "xminoca"; then
        echo "Error: Perl cannot be cross compiled."
        exit 3
    fi

    echo "Copying source to $PWD"
    cp -Rp ${SOURCE_DIRECTORY}/* .
    sh ./Configure -Dprefix="/usr" \
                   -Dcc=$TARGET-gcc \
                   -Accflags="$CFLAGS -fwrapv -fno-strict-aliasing -fno-stack-protector" \
                   -Duse64bitint=y \
                   -Dusemymalloc=n \
                   -Dusenm=n \
                   -Duseshrplib=y \
                   -Dusethreads=y \
                   -Amyhostname=minoca \
                   -Amydomain=minocacorp.com \
                   -Acf_email=info@minocacorp.com \
                   -Aperladmin=info@minocacorp.com \
                   -Alddlflags=-shared \
                   -Astatic_ext=Hash/Util \
                   -d -e

    ;;

  configure-tools)
    echo "Copying source to $PWD"
    cp -Rp ${SOURCE_DIRECTORY}/* .
    if test "x$BUILD_OS" = "xwin32"; then
      OUTPUT_DIRECTORY=`echo $OUTPUT_DIRECTORY | sed 's|/|\\\\\\\\|g'`
      INSTDRIVE=`echo $SRCROOT | sed 's|\([a-zA-Z]:\).*|\1|'`
      tab=`printf '\t'`
      if ! [ -f ./win32/makefile.mk.orig ]; then
        cp -pv ./win32/makefile.mk ./win32/makefile.mk.orig
        sed -e "s|^INST_DRV${tab}.*|INST_DRV *= $INSTDRIVE|" \
            -e "s|^INST_TOP${tab}.*|INST_TOP *= $OUTPUT_DIRECTORY|" \
            -e "s|^#WIN64${tab}.*|WIN64 *= undef|" \
            -e "s|^CCHOME${tab}.*|CCHOME *= $SRCROOT/tools/win32/MinGW|" \
            -e "s|^EMAIL${tab}.*|EMAIL *= info@minocacorp.com|" \
            ./win32/makefile.mk.orig > ./win32/makefile.mk

      fi

    else
      sh ./Configure -Dprefix="$OUTPUT_DIRECTORY" \
                     -Duse64bitint=y \
                     -Dusemymalloc=n \
                     -Dusenm=n \
                     -Dusethreads=y \
                     -Duserelocatableinc=y \
                     -Amyhostname=minoca \
                     -Amydomain=minocacorp.com \
                     -Acf_email=info@minocacorp.com \
                     -Aperladmin=info@minocacorp.com \
                     -d -e
    fi

    ;;

  build)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    OLDPWD=$PWD
    cd "$OUTPUT_DIRECTORY/usr/bin"
    chmod +x perl perlthanks psed pstruct
    cd "$OUTPUT_DIRECTORY/usr/lib"
    ln -s ./perl5/5.20.1/`uname -m`-minoca-thread-multi-64int/CORE/libperl.so ./libperl.so
    cd "$OLDPWD"
    ;;

  build-tools)
    if test "x$BUILD_OS" == "xwin32"; then
      export DMAKEROOT="$OUTPUT_DIRECTORY/share/startup"
      cd ./win32
      unset SHELL
      dmake -f makefile.mk
      dmake -f makefile.mk install

    else
      make $PARALLEL_MAKE
      make $PARALLEL_MAKE install
    fi
  ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac
