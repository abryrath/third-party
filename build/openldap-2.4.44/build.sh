##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the openLDAP suite.
##
## Author:
##
##     Evan Green 2-Sep-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBOPENSSL=libopenssl_1.0.2h
LIBTOOL=libtool_2.2.8

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$LIBOPENSSL"
    extract_dependency "$LIBTOOL"

    export CPPFLAGS="$CPPFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    export AR="$TARGET-ar"
    export ac_cv_func_memcmp_working=yes
    [ "$BUILD_OS" != "minoca" ] && DISABLE_PERL=--disable-perl

    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --disable-static \
                                     --enable-dynamic \
                                     --with-tls=openssl \
                                     --disable-debug \
                                     --enable-crypt \
                                     --enable-slapd \
                                     --enable-modules \
                                     --enable-backends=mod \
                                     --disable-ndb \
                                     --disable-sql \
                                     --disable-shell \
                                     $DISABLE_PERL \
                                     --disable-bdb \
                                     --disable-hdb \
                                     --enable-overlays=mod \
                                     --with-yielding-select=yes \
                                     --with-cyrus-sasl=no \
                                     --prefix="/usr" \
                                     --sysconfdir=/etc \
                                     --libexecdir=/usr/lib \
                                     --localstatedir=/var \
                                     CPPFLAGS="$CPPFLAGS" \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    make $PARALLEL_MAKE V=1
    make $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY" STRIP=
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

