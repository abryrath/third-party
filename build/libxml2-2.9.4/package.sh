##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the XML library.
##
## Author:
##
##     Evan Green 20-Jun-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr/share"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/share/aclocal" "$PACKAGE_DIRECTORY/usr/share"

rm "$PACKAGE_DIRECTORY"/usr/lib/*.la

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libxml2
Depends: libgcc, libz, libiconv, libreadline
Priority: optional
Version: 2.9.4
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: ftp://xmlsoft.org/libxml2/libxml2-2.9.4.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: XML parsing library.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

