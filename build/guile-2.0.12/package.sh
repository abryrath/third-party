##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the guile language.
##
## Author:
##
##     Evan Green 9-Aug-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

rm "$BUILD_DIRECTORY"/usr/lib/*.la
rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/usr/share"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/share/aclocal" "$PACKAGE_DIRECTORY/usr/share"
cp -Rpv "$BUILD_DIRECTORY/usr/share/guile" "$PACKAGE_DIRECTORY/usr/share"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: guile
Depends: libgc, libunistring, libffi, libgmp, libreadline, libtool, libgcc
Priority: optional
Version: 2.0.12
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: ftp://ftp.gnu.org/pub/gnu/guile/guile-2.0.12.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: GNU Guile programming language.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

