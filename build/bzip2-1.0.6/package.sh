##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the bzip2 utility and library.
##
## Author:
##
##     Evan Green 7-Mar-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr/bin"
cp -Rpv "$BUILD_DIRECTORY/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/lib" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/include" "$PACKAGE_DIRECTORY/usr"

##
## Fix the symlinks.
##

OLDPWD=`pwd`
cd "$PACKAGE_DIRECTORY/usr/bin"
rm ./bzcmp ./bzegrep ./bzfgrep ./bzless
ln -s bzmore bzless
ln -s bzgrep bzfgrep
ln -s bzgrep bzegrep
ln -s bzdiff bzcmp

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: bzip2
Priority: optional
Version: 1.0.6
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://www.bzip.org/1.0.6/bzip2-1.0.6.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: bzip2 compression/decompression utility and library.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

