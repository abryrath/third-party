##
## Copyright (c) 2013 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the wget package.
##
## Author:
##
##     Evan Green 17-Sep-2014
##
## Environment:
##
##     Windows Build with POSIX tools.
##

. ../build_common.sh

OPENSSL=libopenssl_1.0.2h
LIBPCRE=libpcre_8.39

##
## Export some variables needed to build correctly on windows.
##

if test "x$BUILD_OS" = "xwin32"; then
    export gl_cv_func_memchr_works=yes
    export gl_cv_func_working_strerror=yes
    export gl_cv_func_strerror_0_works=yes
    export ac_cv_func_malloc_0_nonnull=yes
fi

##
## Don't use pod2man because the pod2man script has #! /usr/bin/env hardcoded
## into it.
##

export ac_cv_path_POD2MAN=no

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    WITH_SSL=--with-ssl=openssl
    extract_dependency "$OPENSSL" || WITH_SSL=--without-ssl
    extract_dependency "$LIBPCRE"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --disable-nls \
                                     $WITH_SSL \
                                     --prefix="/usr" \
                                     --sysconfdir="/etc" \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install-strip prefix="$OUTPUT_DIRECTORY" \
        sysconfdir="$OUTPUT_DIRECTORY/etc"

    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

