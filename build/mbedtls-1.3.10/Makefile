################################################################################
#
#   Copyright (c) 2015 Minoca Corp. All Rights Reserved.
#
#   Module Name:
#
#       mbedtls-1.3.10
#
#   Abstract:
#
#       This makefile is responsible for building the mbedtls (formerly
#       PolarSSL) package.
#
#   Author:
#
#       Evan Green 16-Apr-2015
#
#   Environment:
#
#       Build
#
################################################################################

include ../common.mk

PACKAGE := mbedtls-1.3.10
PATCHED_SOURCE_DIRECTORY := $(OBJROOT)/$(PACKAGE).src
SOURCE_TARBALL := $(ROOT)/third-party/src/$(PACKAGE).tar.gz
ORIGINAL_SOURCE_DIRECTORY := $(OBJROOT)/$(PACKAGE).orig

BUILDROOT := $(OBJROOT)/$(PACKAGE).build
BINROOT := $(BUILDROOT)/build.out
PKGROOT := $(BUILDROOT)/build.pkg

DIFF_FILE := $(CURDIR)/$(PACKAGE).diff

.PHONY: all clean recreate-diff package

all: | $(BINROOT) $(BUILDROOT) $(PATCHED_SOURCE_DIRECTORY)
	@echo Building $(PACKAGE)
	+sh ./build.sh "$(PATCHED_SOURCE_DIRECTORY)" "$(BUILDROOT)" "$(BINROOT)" build
	$(MAKE) package

clean:
	rm -rf $(BUILDROOT)
	rm -rf $(PATCHED_SOURCE_DIRECTORY)
	rm -rf $(ORIGINAL_SOURCE_DIRECTORY)

package:
	sh ./package.sh "$(BINROOT)" "$(PKGROOT)"

##
## This target recreates the diff file from the patched source directory.
##

recreate-diff: $(ORIGINAL_SOURCE_DIRECTORY)
	cd $(PATCHED_SOURCE_DIRECTORY) && diff -Nru3 -x .svn ../$(PACKAGE).orig . > $(DIFF_FILE) || test $$? = "1"

##
## Unpack the source to PACKAGE.orig.
##

$(ORIGINAL_SOURCE_DIRECTORY): $(SOURCE_TARBALL)
	rm -rf $(ORIGINAL_SOURCE_DIRECTORY)
	tar -xzf $^ -C $(OBJROOT)
	mv $(OBJROOT)/$(PACKAGE) $@

##
## Unpack the source to PACKAGE.src, then apply the patch.
##

$(PATCHED_SOURCE_DIRECTORY): $(SOURCE_TARBALL) | $(OBJROOT)
	rm -rf $(PATCHED_SOURCE_DIRECTORY)
	tar -xzf $(SOURCE_TARBALL) -C $(OBJROOT)
	mv $(OBJROOT)/$(PACKAGE) $@
	#cd $(PATCHED_SOURCE_DIRECTORY) && $(PATCH) -p0 -i $(DIFF_FILE)

$(BUILDROOT) $(BINROOT):
	mkdir -p $@

