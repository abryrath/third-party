##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the tmux package.
##
## Author:
##
##     Evan Green 20-Jul-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

NCURSES=libncurses_5.9
LIBEVENT=libevent_2.0.22

##
## Tmux needs a getopt that won't permute arguments.
##

export ac_cv_func_getopt=no

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$NCURSES"
    extract_dependency "$LIBEVENT"
    CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install-strip DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

