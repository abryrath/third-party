##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the croco library.
##
## Author:
##
##     Evan Green 30-Aug-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBICONV=libiconv_1.14
PCRE=libpcre_8.39
LIBFFI=libffi_3.2.1
LIBXML2=libxml2_2.9.4
GETTEXT=gettext_0.19.8.1
LIBGLIB=libglib_2.49.6

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$LIBICONV"
    extract_dependency "$PCRE"
    extract_dependency "$LIBFFI"
    extract_dependency "$GETTEXT"
    extract_dependency "$LIBGLIB"
    extract_dependency "$LIBXML2"
    export CPPFLAGS="$CPPFLAGS -I$DEPENDROOT/usr/include \
 -I$DEPENDROOT/usr/include/glib-2.0 -I$DEPENDROOT/usr/lib/glib-2.0/include \
 -I$DEPENDROOT/usr/include/libxml2"

    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    export CROCO_CFLAGS=" "
    export CROCO_LIBS="-lglib-2.0 -lxml2"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --disable-static \
                                     --prefix="/usr" \
                                     CFLAGS="$CFLAGS" \
                                     CPPFLAGS="$CPPFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install-strip DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

