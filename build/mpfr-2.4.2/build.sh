##
## Copyright (c) 2013 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##              <gmp_package>
##
## Abstract:
##
##     This script configures and makes the MPFR package.
##
## Author:
##
##     Evan Green 24-Oct-2013
##
## Environment:
##
##     Build
##

. ../build_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure-tools)
    export AR='ar'
    GMP_DIRECTORY="$5"
    if ! test -d "$GMP_DIRECTORY"; then
      echo "$0: Error: GMP directory '$GMP_DIRECTORY' must be set and exist."
      exit 1
    fi

    sh ${SOURCE_DIRECTORY}/configure --prefix="$OUTPUT_DIRECTORY" \
                                     --with-gmp="$GMP_DIRECTORY" \
                                     --enable-static \
                                     --disable-shared \
                                     CFLAGS="$CFLAGS"

    ;;

  configure)
    export CC="$TARGET-gcc"
    GMP="$5"
    extract_dependency $GMP
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --prefix="/usr" \
                                     --with-gmp="$DEPENDROOT/usr" \
                                     --enable-static \
                                     --disable-shared \
                                     CFLAGS="$CFLAGS"

    ;;

  build)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY/"
    ;;

  build-tools)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

