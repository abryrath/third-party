##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the GNU TLS package.
##
## Author:
##
##     Evan Green 1-Sep-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBGMP=libgmp_4.3.2
LIBNETTLE=libnettle_3.2
P11KIT=p11kit_0.23.2
LIBICONV=libiconv_1.14
GETTEXT=gettext_0.19.8.1
LIBZ=libz_1.2.8
LIBTASN1=libtasn1_4.9
LIBIDN=libidn_1.33
UNBOUND=unbound_1.5.9

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$LIBGMP"
    extract_dependency "$LIBNETTLE"
    extract_dependency "$P11KIT"
    extract_dependency "$LIBICONV"
    extract_dependency "$GETTEXT"
    extract_dependency "$LIBZ"
    extract_dependency "$LIBTASN1"
    extract_dependency "$LIBIDN"
    extract_dependency "$UNBOUND"

    export CPPFLAGS="$CPPFLAGS -I$DEPENDROOT/usr/include \
 -I$DEPENDROOT/usr/include/p11-kit-1"

    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    export NETTLE_CFLAGS=" "
    export NETTLE_LIBS="-lnettle"
    export HOGWEED_CFLAGS=" "
    export HOGWEED_LIBS="-lhogweed"
    export GMP_CFLAGS=" "
    export GMP_LIBS="-lgmp"
    export LIBTASN1_CFLAGS=" "
    export LIBTASN1_LIBS="-ltasn1"
    export P11_KIT_CFLAGS=" "
    export P11_KIT_LIBS="-lp11-kit"
    export LIBIDN_CFLAGS=" "
    export LIBIDN_LIBS="-lidn"
    export UNBOUND_CFLAGS=" "
    export UNBOUND_LIBS="-lunbound"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --disable-static \
                                     --disable-rpath \
                                     --disable-doc \
                                     --prefix="/usr" \
                                     --with-libiconv-prefix="$DEPENDROOT/usr" \
                                     --with-libintl-prefix="$DEPENDROOT/usr" \
                                     --with-default-trust-store-file=/etc/ssl/ca-bundle.crt \
                                     CFLAGS="$CFLAGS" \
                                     CPPFLAGS="$CPPFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

