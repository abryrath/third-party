##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the bison utility.
##
## Author:
##
##     Evan Green 16-Feb-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr/bin"
mkdir -p "$PACKAGE_DIRECTORY/usr/lib"
cp -pv "$BUILD_DIRECTORY/bin/bison" "$PACKAGE_DIRECTORY/usr/bin"
cp -pv "$BUILD_DIRECTORY/lib/liby.a" "$PACKAGE_DIRECTORY/usr/lib"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: bison
Priority: optional
Version: 3.0.2
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://ftp.gnu.org/gnu/bison/bison-3.0.2.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Bison is a general-purpose parser generator that converts an
 annotated context-free grammar into a deterministic LR or generalized LR (GLR)
 parser employing LALR(1) parser tables.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

