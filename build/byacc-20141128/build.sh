##
## Copyright (c) 2014 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the Berkeley yacc package.
##
## Author:
##
##     Evan Green 2-May-2014
##
## Environment:
##
##     Build
##

. ../build_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure-tools)
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --prefix="/" \
                                     --program-transform-name=':'

    ;;

  configure)
    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     --program-transform-name=':' \
                                     CFLAGS="$CFLAGS"

    ;;

  build-tools)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install prefix="$OUTPUT_DIRECTORY"
    ;;

  build)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install prefix="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

