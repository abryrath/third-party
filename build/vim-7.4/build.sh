##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the vim package.
##
## Author:
##
##     Evan Green 2-Nov-2015
##
## Environment:
##
##     Build
##

. ../build_common.sh

NCURSES=libncurses_5.9

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$NCURSES"
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    export vim_cv_toupper_broken=no
    export vim_cv_terminfo=yes
    export vim_cv_tty_group=world
    export vim_cv_getcwd_broken=no
    export vim_cv_stat_ignores_slash=no
    export vim_cv_memmove_handles_overlap=yes
    export vim_cv_bcopy_handles_overlap=yes
    export vim_cv_memcpy_handles_overlap=no
    export ac_cv_c_bigendian=no
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --disable-nls \
                                     --disable-darwin \
                                     --prefix="/usr" \
                                     --srcdir="${SOURCE_DIRECTORY}/src" \
                                     --with-tlib=ncurses \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    export UNAME=Minoca
    make $PARALLEL_MAKE \
         NL='"\\015\\012"' CC="$TARGET-gcc -I$DEPENDROOT/usr/include"

    make install \
        DESTDIR="$OUTPUT_DIRECTORY" STRIP="$TARGET-strip"

    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

