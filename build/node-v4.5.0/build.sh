##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the node.js package.
##
## Author:
##
##     Evan Green 17-Aug-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

OPENSSL=libopenssl_1.0.2h
ZLIB=libz_1.2.8

export CC="$TARGET-gcc"
export CXX="$TARGET-g++"
export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include \
-I$SOURCE_DIRECTORY/deps/uv/include"

export CXXFLAGS="$CFLAGS"
export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

export LD_LIBRARY_PATH="$DEPENDROOT/usr/lib"
cd $SOURCE_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$OPENSSL"
    extract_dependency "$ZLIB"

    case "$ARCH" in
    x86) ARCH_FLAGS=--dest-cpu=x86 ;;
    armv6)
      ARCH_FLAGS="--dest-cpu=arm --with-arm-float-abi=hard --with-arm-fpu=vfp"
      ;;

    armv7)
      ARCH_FLAGS="--dest-cpu=arm --with-arm-float-abi=hard \
--with-arm-fpu=vfpv3-d16"
      ;;

    x64) ARCH_FLAGS=--dest-cpu=x64 ;;
    *)
      echo "Unsupported architecture!"
      exit 3
      ;;

    esac

    OPENSSL_FLAGS="--shared-openssl \
--shared-openssl-includes=$DEPENDROOT/usr/include
--shared-openssl-libpath=$DEPENDROOT/usr/lib"

    ${SOURCE_DIRECTORY}/configure --prefix="/usr" \
                                  --dest-os=minoca \
                                  $ARCH_FLAGS \
                                  $OPENSSL_FLAGS \
                                  --shared-libuv \
                                  --download=none \

    ##
    ## Node was configured to use a shared libuv. Also set up
    ## libuv to be built.
    ##

    export PYTHONPATH="$SOURCE_DIRECTORY/tools/gyp/pylib"
    cd "$SOURCE_DIRECTORY/deps/uv"
    ./gyp_uv.py -Duv_library=shared_library
    ;;

  build)

    ##
    ## Build libuv first.
    ##

    cd "$SOURCE_DIRECTORY/deps/uv/out"
    make $PARALLEL_MAKE libuv.so.1 V=1

    ##
    ## Copy the resulting library into the deps
    ## directory so the rest of node can find it.
    ##

    cp -pv ./Debug/lib.target/libuv.so.1 $DEPENDROOT/usr/lib/
    ln -sf libuv.so.1 $DEPENDROOT/usr/lib/libuv.so
    mkdir -p "$OUTPUT_DIRECTORY/libuv/usr/lib/"
    cp -pv ./Debug/lib.target/libuv.so.1 "$OUTPUT_DIRECTORY/libuv/usr/lib"
    ln -sf libuv.so.1 "$OUTPUT_DIRECTORY/libuv/usr/lib/libuv.so"
    cp -Rpv ../include "$OUTPUT_DIRECTORY/libuv/usr/"

    ##
    ## Now make the rest of node.
    ##

    cd "$SOURCE_DIRECTORY"
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

