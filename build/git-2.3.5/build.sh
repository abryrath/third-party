##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the git package.
##
## Author:
##
##     Evan Green 16-Apr-2015
##
## Environment:
##
##     Build
##

. ../build_common.sh

ZLIB=libz_1.2.8
CURL=curl_7.41.0
LIBICONV=libiconv_1.14
SUBVERSION=subversion_1.8.11
EXPAT=expat_2.1.0

##
## Export some variables needed to build correctly on windows.
##

if test "$BUILD_OS" != "minoca"; then
    export ac_cv_fread_reads_directories=no
    export ac_cv_snprintf_returns_bogus=no
    export SHELL_PATH=sh
fi

unset DIFF || true

cd $SOURCE_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency "$ZLIB"
    extract_dependency "$CURL"
    extract_dependency "$LIBICONV"
    extract_dependency "$SUBVERSION" || echo "Ignoring subversion"
    extract_dependency "$EXPAT"
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     --with-gitconfig=/etc/gitconfig \
                                     --with-expat \
                                     CFLAGS="$CFLAGS"

    ;;

  build)
    export V=1
    if test "$BUILD_OS" != "minoca"; then
        export NO_PERL=1
    fi

    export NO_TCLTK=1
    make $PARALLEL_MAKE uname_S=Minoca NO_INSTALL_HARDLINKS=YesPlease
    make $PARALLEL_MAKE uname_S=Minoca install DESTDIR="$OUTPUT_DIRECTORY" \
        NO_INSTALL_HARDLINKS=YesPlease

    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

