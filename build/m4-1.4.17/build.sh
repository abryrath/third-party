##
## Copyright (c) 2013 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the m4 package.
##
## Author:
##
##     Evan Green 28-Oct-2013
##
## Environment:
##
##     Windows Build with POSIX tools.
##

. ../build_common.sh


cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure-tools)
    sh ${SOURCE_DIRECTORY}/configure --prefix="$OUTPUT_DIRECTORY"
    ;;

  configure)
    export CC="$TARGET-gcc"

    ##
    ## Export some variables needed to build correctly on windows.
    ##

    if test "x$BUILD_OS" = "xwin32"; then
        export ac_cv_func_malloc_0_nonnull=yes
        export ac_cv_func_realloc_0_nonnull=yes
        export gl_cv_func_fflush_stdin=yes
        export gl_cv_func_strerror_0_works=yes
        export gl_cv_func_strstr_works_always=yes
        export gl_cv_func_memchr_works=yes
        export gl_cv_func_working_mkstemp=yes
        export gl_cv_func_strstr_linear=yes
    fi

    CFLAGS="$CFLAGS -DSLOW_BUT_NO_HACKS -DOK_TO_USE_1S_CLOCK"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     CFLAGS="$CFLAGS"

    ;;

  build | build-tools)
    touch ${SOURCE_DIRECTORY}/doc/*.1
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install-strip prefix="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

