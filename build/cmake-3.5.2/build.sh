##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the cmake package.
##
## Author:
##
##     Chris Stevens 22-Jun-2016
##
## Environment:
##
##     Windows Build with POSIX tools.
##

. ../build_common.sh

##
## Export the flags so that they can get picked up by either the toolchain
## file or the bootstrap script.
##

export CFLAGS="$CFLAGS"
export CXXFLAGS="$CFLAGS"
export LDFLAGS="$LDFLAGS"

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure-tools)
    sh ${SOURCE_DIRECTORY}/bootstrap --prefix="/"
    ;;

  configure)

    ##
    ## When cross-compiling on Windows, assume that tools has already been
    ## built and use the version of cmake that runs on Windows.
    ##

    if test "x$BUILD_OS" = "xwin32"; then
        cmake -G "Unix Makefiles" \
              -D CMAKE_INSTALL_PREFIX="/usr" \
              -D CMAKE_TOOLCHAIN_FILE="$BUILD_DIRECTORY/minoca_toolchain.cmake" \
              ${SOURCE_DIRECTORY}

    ##
    ## Otherwise just bootstrap a native version of cmake, rather than relying
    ## on a cross-compiled version.
    ##

    else
        if [ "$PARALLEL_MAKE" ]; then
            CMAKE_PARALLEL=--parallel=`echo $PARALLEL_MAKE | sed 's/-j//'`
        fi

        sh ${SOURCE_DIRECTORY}/bootstrap $CMAKE_PARALLEL --prefix="/usr"
    fi

    ;;

  build-tools)
    make $PARALLEL_MAKE COLOR=0
    make $PARALLEL_MAKE install/strip DESTDIR="$OUTPUT_DIRECTORY" COLOR=0
    ;;

  build)
    make $PARALLEL_MAKE COLOR=0
    make $PARALLEL_MAKE install/strip DESTDIR="$OUTPUT_DIRECTORY" COLOR=0
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

