##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the awk utility.
##
## Author:
##
##     Evan Green 13-Feb-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr/bin"
cp -pv "$BUILD_DIRECTORY/awk" "$PACKAGE_DIRECTORY/usr/bin/awk"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: awk
Priority: optional
Version: 20121220
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://www.cs.princeton.edu/~bwk/btl.mirror/awk.zip
Installed-Size: $INSTALLED_SIZE
Description: awk is a small text manipulation utility and programming language
 written by Brian Kernighan.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

