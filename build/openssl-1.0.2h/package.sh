##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the openssl library.
##
## Author:
##
##     Evan Green 16-Feb-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

rm -rf "$PACKAGE_DIRECTORY/usr" "$PACKAGE_DIRECTORY/etc"
mkdir -p "$PACKAGE_DIRECTORY"
cp -Rpv "$BUILD_DIRECTORY/usr" "$PACKAGE_DIRECTORY"
cp -Rpv "$BUILD_DIRECTORY/etc" "$PACKAGE_DIRECTORY/etc"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libopenssl
Depends: ca-certificates
Priority: optional
Version: 1.0.2h
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://www.openssl.org/source/openssl-1.0.2h.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: The OpenSSL library implements support for Secure Sockets Layer
 (SSL v2/v3) and Transport Layer Security (TLS) protocols as well as a set of
 full-strength general purpose cryptographic functions.
_EOS

cat > "$PACKAGE_DIRECTORY/CONTROL/conffiles" <<_EOS
/etc/ssl/openssl.cnf
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

