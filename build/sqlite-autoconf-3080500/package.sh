##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the SQLite utility.
##
## Author:
##
##     Evan Green 16-Feb-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/lib" "$PACKAGE_DIRECTORY/usr"

##
## Libtool sucks.
##

rm "$PACKAGE_DIRECTORY/usr/lib/libsqlite3.la"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: sqlite
Priority: optional
Version: 3080500
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://www.sqlite.org/2014/sqlite-autoconf-3080500.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: The SQLite library contains a lightweight SQL database.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

