##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the shadow package.
##
## Author:
##
##     Evan Green 3-Mar-2015
##
## Environment:
##
##     Build.
##

. ../build_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export shadow_cv_utmpdir=/var/run
    export shadow_cv_logdir=/var/log
    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --disable-nls \
                                     --enable-utmpx \
                                     --enable-shared \
                                     --with-nscd=no \
                                     --with-group-name-max-length=32 \
                                     --enable-subordinate-ids=no \
                                     --prefix="/usr" \
                                     --exec_prefix="/usr" \
                                     --sysconfdir="/etc"
                                     CFLAGS="$CFLAGS"

    ;;

  build)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install-strip prefix="$OUTPUT_DIRECTORY" \
                        exec_prefix="$OUTPUT_DIRECTORY" \
                        sysconfdir="$OUTPUT_DIRECTORY/etc"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

